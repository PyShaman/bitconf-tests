import requests
import pytest

from assertpy import assert_that


class TestStatusEndpointSecurity:

    def test_01_verify_get_status_endpoint(self):
        response = requests.put("http://127.0.0.1:8080/status/",
                                headers={
                                    "accept": "application/json",
                                    "Content-Type": "application/json"
                                },
                                json={})
        assert_that(response.status_code).is_not_equal_to(200)

    def test_02_verify_get_method_override(self):
        response = requests.post("http://127.0.0.1:8080/status",
                                 headers={
                                     "accept": "application/json",
                                     "Content-Type": "application/json",
                                     "X-HTTP-Method-Override": "PUT"
                                 },
                                 json={})
        print(response.status_code)
        assert_that(response.status_code).is_not_equal_to(200)
