import pytest
import requests

from assertpy import assert_that, soft_assertions


@pytest.mark.usefixtures("set_status")
@pytest.mark.parametrize("set_status", [{"status": "ONLINE"}], indirect=True)
class TestStatusEndpoint:

    def test_01_verify_status_endpoint_status_code_200(self):
        response = requests.get("http://127.0.0.1:8080/status",
                                headers={"accept": "application/json"})
        with soft_assertions():
            assert_that(response.status_code).is_equal_to(200)
            assert_that(response.json()["data"]["status"]).is_equal_to("ONLINE")

    def test_02_critical_path(self):
        response_1 = requests.get("http://127.0.0.1:8080/status",
                                  headers={"accept": "application/json"})
        response_2 = requests.post("http://127.0.0.1:8080/status/",
                                   headers={
                                       "accept": "application/json",
                                       "Content-Type": "application/json"
                                   },
                                   json={
                                       "status": "OFFLINE"
                                   })
        response_3 = requests.get("http://127.0.0.1:8080/status",
                                  headers={"accept": "application/json"})
        with soft_assertions():
            assert_that(response_1.status_code).is_equal_to(200)
            assert_that(response_1.json()["data"]["status"]).is_equal_to("ONLINE")
            assert_that(response_2.status_code).is_equal_to(200)
            assert_that(response_2.json()["data"]["status"]).is_equal_to("OFFLINE")
            assert_that(response_3.status_code).is_equal_to(200)
            assert_that(response_3.json()["data"]["status"]).is_equal_to("OFFLINE")
