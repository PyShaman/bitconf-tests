import requests
import pytest

from assertpy import assert_that, soft_assertions


@pytest.mark.usefixtures("access_token", "attendee_data", "create_and_delete_attendee")
class TestAttendees:

    def test_01_create_attendee(self, access_token):
        ca = requests.post("http://127.0.0.1:8080/attendee/",
                           headers={
                               "accept": "application/json",
                               "Content-Type": "application/json",
                               "Authorization": f"Bearer {access_token}"
                           },
                           json={
                               "fullname": "Adam Lange",
                               "email": "alange@example.com",
                               "job_description": "I am security engineer",
                               "experience": 20,
                               "seniority": "extreme unicorn!"
                           })
        with soft_assertions():
            assert_that(ca.status_code).is_equal_to(200)
            assert_that(ca.json()["data"]["fullname"]).is_equal_to("Adam Lange")

    def test_02_post_attendee_random_data(self, access_token, attendee_data):
        ca = requests.post("http://127.0.0.1:8080/attendee/",
                           headers={
                               "accept": "application/json",
                               "Content-Type": "application/json",
                               "Authorization": f"Bearer {access_token}"
                           },
                           json=attendee_data)
        with soft_assertions():
            assert_that(ca.status_code).is_equal_to(200)
            assert_that(ca.json()["data"]["fullname"]).is_equal_to(attendee_data["fullname"])
            assert_that(ca.json()["data"]["email"]).is_equal_to(attendee_data["email"])
            assert_that(ca.json()["data"]["job_description"]).is_equal_to(attendee_data["job_description"])
            assert_that(ca.json()["data"]["experience"]).is_equal_to(attendee_data["experience"])
            assert_that(ca.json()["data"]["seniority"]).is_equal_to(attendee_data["seniority"])

    def test_03_get_existing_attendee(self, access_token, create_and_delete_attendee):
        attendee_json, attendee_id = create_and_delete_attendee
        new_attendee = requests.get(f"http://127.0.0.1:8080/attendee/{attendee_id}",
                                    headers={
                                        "accept": "application/json",
                                        "Content-Type": "application/json",
                                        "Authorization": f"Bearer {access_token}"
                                    })
        with soft_assertions():
            assert_that(new_attendee.status_code).is_equal_to(200)
            assert_that(new_attendee.json()["data"]).is_equal_to(attendee_json["data"])
