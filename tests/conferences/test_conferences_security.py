import requests
import pytest

from assertpy import assert_that


class TestConferencesSecurity:

    def test_01_enumerate_over_conference_id(self):
        conference_ids = [
            "632ecb66d55a5d9de542d2e1",
            "632ecb66d55a5d9de542d2eb",
            "632ecb66d55a5d9de542d2ec",
            "632ecb66d55a5d9de542d2ed",
            "632ecb66d55a5d9de542d2ee",
            "632ecb66d55a5d9de542d2ef",
            "632ecb66d55a5d9de542d2eg",
            "632ecb66d55a5d9de542d2eh",
        ]
        for id_ in conference_ids:
            response = requests.get(f"http://127.0.0.1:8080/conference/{id_}",
                                    headers={
                                        "accept": "application/json",
                                        "Content-Type": "application/json"
                                    })
            if response.status_code == 200:
                print(f"IDOR found for id {id_}")
            assert_that(response.status_code).is_not_equal_to(200)

    def test_02_delete_conference(self):
        conference_ids = [
            "632ecb66d55a5d9de542d2e1",
            "632ecb66d55a5d9de542d2eb",
            "632ecb66d55a5d9de542d2ec",
            "632ecb66d55a5d9de542d2ed",
            "632ecb66d55a5d9de542d2ee",
            "632ecb66d55a5d9de542d2ef",
            "632ecb66d55a5d9de542d2eg",
            "632ecb66d55a5d9de542d2eh",
        ]
        for id_ in conference_ids:
            response = requests.delete(f"http://127.0.0.1:8080/conference/{id_}",
                                       headers={
                                           "accept": "application/json",
                                           "Content-Type": "application/json"
                                       })
            if response.status_code == 200:
                print(f"IDOR found for id {id_}")
            assert_that(response.status_code).is_not_equal_to(200)
