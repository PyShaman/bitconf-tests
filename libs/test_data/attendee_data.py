import random

from dataclasses import asdict, field, dataclass

from faker import Faker


@dataclass
class AttendeeData:
    jd = ["Manual Tester", "Automated QA", "QA Engineer", "Java Developer", "COBOL Unicorn"]
    senior = ["junior", "mid", "senior"]

    fk = Faker("pl_PL")
    fullname: str = field(default=fk.name())
    email: str = field(default=fk.free_email())
    job_description: str = field(default=jd[random.randint(0, 4)])
    experience: int = field(default=random.randint(1, 10))
    seniority: str = field(default=senior[random.randint(0, 2)])

    def return_data(self):
        return asdict(self)
