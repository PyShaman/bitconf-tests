import pytest
import requests

from assertpy import assert_that, soft_assertions


@pytest.fixture(scope="class")
def set_status(request):
    app_status = requests.post("http://127.0.0.1:8080/status/",
                               headers={
                                   "accept": "application/json",
                                   "Content-Type": "application/json"
                               },
                               json={
                                   "status": request.param["status"]
                               })
    with soft_assertions():
        assert_that(app_status.status_code).is_equal_to(200)
        assert_that(app_status.json()["data"]["status"]).is_equal_to(request.param["status"])
