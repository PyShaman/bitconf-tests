import pytest

from libs.test_data.attendee_data import AttendeeData


@pytest.fixture
def attendee_data():
    return AttendeeData().return_data()
