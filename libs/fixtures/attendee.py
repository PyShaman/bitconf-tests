import requests
import pytest

from assertpy import assert_that

from libs.test_data.attendee_data import AttendeeData


@pytest.fixture
@pytest.mark.usefixtures("access_token")
def create_and_delete_attendee(access_token):
    attendee = requests.post("http://127.0.0.1:8080/attendee/",
                             headers={
                                 'accept': 'application/json',
                                 'Content-Type': 'application/json',
                                 'Authorization': f'Bearer {access_token}'
                             },
                             json=AttendeeData().return_data())
    assert_that(attendee.status_code).is_equal_to(200)
    attendee_id = attendee.json()["data"]["id"]
    yield attendee.json(), attendee_id
    delete_attendee = requests.delete(f"http://127.0.0.1:8080/attendee/{attendee_id}",
                                      headers={
                                          'accept': 'application/json',
                                          'Content-Type': 'application/json',
                                          'Authorization': f'Bearer {access_token}'
                                      })
    assert_that(delete_attendee.status_code).is_equal_to(200)
