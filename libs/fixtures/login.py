import requests
import pytest

from assertpy import assert_that


@pytest.fixture(scope="class")
def access_token():
    at = requests.post("http://127.0.0.1:8080/login",
                       headers={
                           "accept": "application/json",
                           "Content-Type": "application/json"
                       },
                       json={
                           "username": "admin",
                           "password": "admin"
                       })
    assert_that(at.status_code).is_equal_to(200)
    return at.json()["data"]["access_token"]
